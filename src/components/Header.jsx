import { View, TouchableOpacity, Text, StyleSheet } from "react-native";

const options = [
  "Pomodoro", "Short break", "Long break"
];
export function Header({
  currentTime,
  setCurrentTime,
  setTime
}) {

  const handlePress = (index) => {
    const newTime = index === 0 ? 25 : index == 1 ? 5 : 15;
    setCurrentTime(index);
    setTime(newTime * 60);
  };

  return (
    <View style={styles.viewHeader}>
      {options.map((item, index) => (
        <TouchableOpacity
          onPress={() => handlePress(index)}
          style={[
            styles.tabItem,
            currentTime !== index && { borderColor: "transparent" }
          ]}
          key={index}
        >
          <Text style={{ fontWeight: "bold" }}>{item}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  viewHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  tabItem: {
    borderWidth: 3,
    padding: 5,
    flex: 1,
    borderColor: "white",
    alignItems: "center",
    borderRadius: 15,
    marginVertical: 20
  },
});