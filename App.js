import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity } from 'react-native';
import { useEffect, useState } from 'react';
import { Header, Timer } from './src/components';
import { Audio } from 'expo-av';
/**
 * SafeAreaView only IOS
 * Platform: OS identify
 * StatusBar: StatusBar styles
 */
const colors = ["#F7DC6F", "#A2D9CE", "#D7BDE2"];

export default function App() {
  const [isActive, setIsActive] = useState(false);
  const [isWorking, setIsWorking] = useState(false);
  const [time, setTime] = useState(25 * 60);
  const [currentTime, setCurrentTime] = useState("POMO" | "SHORT" | "LONG");

  const handleStartStop = () => {
    playSound();
    setIsActive(!isActive);
  };

  const playSound = async() => {
    const { sound } = await Audio.Sound.createAsync(
      require("./assets/click.mp3")
    );
    await sound.playAsync();
  };

  const playEndTime = async() => {
    const { sound } = await Audio.Sound.createAsync(
      require("./assets/clock-alarm.mp3")
    );
    await sound.playAsync();
  }

  useEffect(() => {
    let interval = null;

    if(isActive) {
      interval = setInterval(() => {
        setTime(time - 1);
      }, 1000);
    } else {
      clearInterval(interval);
    }

    if (time === 0) {
      setIsActive(false);
      setIsWorking(prev => !prev); // changed state in same cycle
      setTime(isWorking ? 300 : 1500);
      playEndTime();
    }

    return () => clearInterval(interval);
  }, [isActive, time]);

  return (
    <SafeAreaView style={[
      styles.container,
      { backgroundColor: colors[currentTime] }
    ]}>
      <View style={{
        flex:1,
        paddingHorizontal:15
      }}>
        <Text style={styles.textTitle}>Pomodoro</Text>
        <Header
          currentTime={currentTime}
          setCurrentTime={setCurrentTime}
          setTime={setTime}
        />
        <Timer time={time} />
        <TouchableOpacity onPress={() => handleStartStop()} style={styles.button}>
          <Text style={{color: "white", fontWeight: "bold"}}>
            {isActive ? "STOP" : "START"}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textTitle: {
    paddingTop: 8,
    fontSize: 34,
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "#333",
    padding: 15,
    marginTop: 15,
    borderRadius: 15,
    alignItems: "center"
  }
});
